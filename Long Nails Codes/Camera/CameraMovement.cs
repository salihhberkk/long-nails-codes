using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] GameObject player;
    public Vector3 offset;

    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
    public GameObject GetCharacter()
    {
        return player;
    }
}
