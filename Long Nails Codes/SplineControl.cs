using UnityEngine;
using Dreamteck.Splines;
using DG.Tweening;
public class SplineControl : MonoBehaviour
{
    SplinePoint startPoint;
    public static int splineCounter = 0;

    SplineFollower follower;

    GameObject player;
    private void Start()
    {    
        follower = FindObjectOfType<SplineFollower>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            var splines = Level.Instance.splines;
            player = other.gameObject;
            
            if (!GameManager.Instance.leftNails[0].activeSelf)
            {
                player.GetComponent<PlayerMovement>().Die();
                return;
            }
            other.GetComponent<SplineFollower>().spline = splines[splineCounter];
            startPoint = splines[splineCounter].GetPoint(0);
            other.transform.DOMove(startPoint.position, 0.5f)
                .OnComplete(() => OpenFollow());

            splineCounter++;
        }
    }
    public void OpenFollow()
    {
        player.GetComponent<SplineFollower>().Restart(0);
        player.GetComponent<SplineFollower>().follow = true;
    
    }
    public void CloseFollow()
    {
        player.GetComponent<SplineFollower>().follow = false;
        player.transform.rotation = Quaternion.Euler(0, 0, 0);
        player.GetComponent<PlayerMovement>().TPoseControl(false);
    }
    public void NormalSplineStart()
    {
        player.GetComponent<PlayerMovement>().TPoseControl(true);
    }
    public void LastSplineEnd()
    {
        player.GetComponent<SplineFollower>().follow = false;
        player.GetComponent<PlayerMovement>().WinDance();
    }
    public void LastSplineStart()
    {
        player.GetComponent<PlayerMovement>().StopSwipeandRun();
    }
}
