using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Shuriken : MonoBehaviour
{
    PlayerMovement playerMovement;
    public float animDuration;
    public Ease animEase;

    private void Start()
    {
        playerMovement = FindObjectOfType<PlayerMovement>();

        transform.DORotate(new Vector3(0, 80, 180), animDuration)
            .SetEase(animEase)
            .SetLoops(-1, LoopType.Incremental);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerMovement.Die();
        }
        if (other.gameObject.CompareTag("Nail"))
        {
            GameManager.Instance.CutNail(other.gameObject);
            
        }
    }
}
