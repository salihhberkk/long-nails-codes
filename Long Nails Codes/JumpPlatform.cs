using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPlatform : MonoBehaviour
{
    public float jumpHeight;
    public void Jump(GameObject player)
    {
        player.GetComponent<Rigidbody>().AddForce(Vector3.up * jumpHeight );
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Jump(collision.gameObject);
        }
    }
}
