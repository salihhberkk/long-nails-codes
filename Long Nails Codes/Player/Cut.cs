﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;

public class Cut : MonoBehaviour
{
    Material mat;
    GameObject cuttingObject;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Cuttable"))
        {
            mat = other.gameObject.GetComponent<MeshRenderer>().material;
            cuttingObject = other.gameObject.gameObject;
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            SlicedHull cutOff = CutMethod(cuttingObject, mat);

            GameObject cuttingUp = cutOff.CreateUpperHull(cuttingObject, mat);
            cuttingUp.AddComponent<MeshCollider>().convex = true;
            cuttingUp.AddComponent<Rigidbody>();
            cuttingUp.layer = LayerMask.NameToLayer("Cuttable");
            cuttingUp.gameObject.tag = "Enemy";


            GameObject cuttingDown = cutOff.CreateLowerHull(cuttingObject, mat);
            cuttingDown.AddComponent<MeshCollider>().convex = true;
            cuttingDown.AddComponent<Rigidbody>();
            cuttingDown.layer = LayerMask.NameToLayer("Cuttable");
            cuttingDown.gameObject.tag = "Enemy";
            cuttingDown.GetComponent<Rigidbody>().AddExplosionForce(5f, Vector3.up + Vector3.right, 5f);
            Destroy(cuttingObject);
        }
    }
    public SlicedHull CutMethod(GameObject obj, Material crossSectionMaterial = null)
    {
        return obj.Slice(transform.position + new Vector3(0, 1f, 0), transform.up, crossSectionMaterial);
    }

}
