using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;
using Dreamteck.Splines;
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float swipeSensitivity;

    bool stopMove = false;
    SplineFollower follower;
    public Collider mainCollider;
    public Rigidbody mainRig;
    Animator animator;
    Rigidbody rb;

    CharacterState characterState;
    enum CharacterState
    {
        Idle = 0,
        Walk = 1,
        Dance = 2,
        TPose = 3,
    }
    private void Awake()
    {
        GetRagdollBits();
        RagdollModeOff();
        follower = GetComponent<SplineFollower>();
        animator = GetComponent<Animator>();
    }
    private void Start()
    {
        SetState(CharacterState.Idle);
        rb = GetComponent<Rigidbody>();
        StopSwipeandRun();
    }
    void FixedUpdate()
    {
        if (follower.follow == false && stopMove)
        {
            transform.Translate(Vector3.forward * speed);
        }
    }
    private void HandleHead(LeanFinger obj)
    {
        var targetPos = Vector3.right * (obj.ScaledDelta.x * swipeSensitivity / 100);
        if (follower.follow == false)
        {
            transform.position += targetPos;

            transform.position = new Vector3(CalculateClamp(transform.position.x, -3, 3), transform.position.y, transform.position.z);
        }
        else
        {

            follower.motion.offset += new Vector2(targetPos.x, targetPos.y);
            follower.motion.offset = new Vector2(CalculateClamp(follower.motion.offset.x, -0.5f, 0.5f), 0);

        }

    }
    public float CalculateClamp(float value, float min, float max)
    {
        return Mathf.Clamp(value, min, max);
    }
    public void StartSwipeandRun()
    {
        SetState(CharacterState.Walk);
        LeanTouch.OnFingerUpdate += HandleHead;
        stopMove = true;

    }
    public void StopSwipeandRun()
    {
        LeanTouch.OnFingerUpdate -= HandleHead;
        stopMove = false;
    }
    public void LastRun()
    {
        LeanTouch.OnFingerUpdate -= HandleHead;
    }
    public void WinDance()
    {
        SetState(CharacterState.Dance);

        foreach (Collider col in mainCollider.GetComponentsInChildren<Collider>())
        {
            if (col.CompareTag("Nail"))
            {
                col.enabled = false;
            }
        }
    }
    private void SetState(CharacterState state)
    {
        characterState = state;

        animator.SetInteger("Walk", (int)state);
    }
    public void Die()
    {
        StopSwipeandRun();
        RagdollModeOn();
        UIManager.Instance.ShowPanel(PanelType.Lose);
    }
    public void TPoseControl(bool value)
    {
        if (value == true)
        {
            SetState(CharacterState.TPose);
        }
        else
        {
            SetState(CharacterState.Walk);
        }
    }
    public void RagdollModeOn()
    {
        animator.enabled = false;
        foreach (Collider col in ragDollColliders)
        {
            col.enabled = true;
        }
        foreach (Rigidbody rigid in limbsRigidbodies)
        {
            rigid.isKinematic = false;
        }
        mainCollider.enabled = false;
        mainRig.isKinematic = true;
    }
    public void RagdollModeOff()
    {
        foreach (Collider col in ragDollColliders)
        {
            if (!col.CompareTag("Nail"))
            {
                col.enabled = false;
            }
        }
        foreach (Rigidbody rigid in limbsRigidbodies)
        {
            rigid.isKinematic = true;
        }
        mainCollider.enabled = true;
        mainRig.isKinematic = false;
    }
    Collider[] ragDollColliders;
    Rigidbody[] limbsRigidbodies;
    public void GetRagdollBits()
    {
        ragDollColliders = mainRig.GetComponentsInChildren<Collider>();
        limbsRigidbodies = mainRig.GetComponentsInChildren<Rigidbody>();
    }
}
