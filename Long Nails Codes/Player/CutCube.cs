using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutCube : MonoBehaviour
{
    [SerializeField] GameObject splitObject;
    [SerializeField] GameObject particle;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Nail"))
        {
            gameObject.SetActive(false);
            splitObject.SetActive(true);
            
            Instantiate(particle, splitObject.transform.position + new Vector3(0,2f,0), Quaternion.identity);
            //downCubeObj.GetComponent<Rigidbody>().AddForce(Vector3.up * 1500);
        }
    }
}
