﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T>: MonoBehaviour where T: MonoBehaviour
{
    private static T instance;

    public void Awake()
    {
        if (instance != this && instance != null)
        {
            Destroy(gameObject);
        }
    }
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();
            }
            if (instance == null)
            {
                instance = new GameObject().AddComponent<T>();
            }
            


            return instance;
        }
    }
}
