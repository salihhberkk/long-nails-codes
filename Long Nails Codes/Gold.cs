using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour
{
    [SerializeField] GameObject particle;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameManager.Instance.SetGoldText();
            gameObject.SetActive(false);
            Instantiate(particle, transform.position , Quaternion.identity);
        }
    }
}
