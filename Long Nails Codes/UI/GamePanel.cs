using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePanel : Panel
{
    [SerializeField] private Image progressBar;
    [SerializeField] private TextMeshProUGUI currentLevelText;
    [SerializeField] private TextMeshProUGUI nextLevelText;

    [SerializeField] private GameObject player;

    FinishLine line;
    public GamePanel()
    {
        panelType = PanelType.Game;
    }
    private void Start()
    {
        line = FindObjectOfType<FinishLine>();
    }
    private void Update()
    {
        FillBar();
    }
    public void FillBar()
    {
        float fill;
        fill = Map(player.transform.position.z, 0, line.transform.position.z, 0, 1);
        progressBar.fillAmount = fill;
    }
    private void OnEnable()
    {
        currentLevelText.text = LevelManager.Instance.LevelIndicatorIndex.ToString();
        nextLevelText.text = (LevelManager.Instance.LevelIndicatorIndex + 1).ToString();
    }

    public void RestartLevel()
    {
        UIManager.Instance.RetryLevel();
    }
    float Map(float s, float a1, float a2, float b1, float b2)
    {
        return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
    }
}