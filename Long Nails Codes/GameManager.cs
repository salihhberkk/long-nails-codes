using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameManager : MonoSingleton<GameManager>
{
    [SerializeField] Text goldText;
    public int currentGold;
    PlayerMovement player;
    public GameObject characterObj;

    public int nailStack = 0;
    int counter;

    public GameObject[] leftNails;
    public GameObject[] rightNails;

    [SerializeField] GameObject cutNailParticle;
    private void Start()
    {

        currentGold = PlayerPrefs.GetInt("Gold", 0);
        SetGoldText();
        player = FindObjectOfType<PlayerMovement>();
    }
    public void SetGoldText()
    {
        PlayerPrefs.SetInt("Gold", currentGold);
        goldText.text = currentGold.ToString();
        currentGold += 1;
    }
    internal void StartGame()
    {
        UIManager.Instance.ShowPanel(PanelType.Game);
        player.StartSwipeandRun();
    }
    public void AddNail()
    {
        Debug.Log(nailStack);
        if (nailStack < leftNails.Length - 1 && nailStack >= 0)
        {
            leftNails[nailStack + 1].SetActive(true);
            rightNails[nailStack + 1].SetActive(true);
            nailStack++;
        }
    }
    public void SubtractNail()
    {
        if (nailStack < 1)
        {
            player.Die();
        }
        else
        {
            InstantiateParticle(nailStack);
            leftNails[nailStack].SetActive(false);
            rightNails[nailStack].SetActive(false);
            nailStack--;
        }
    }
    public void CutNail(GameObject obj)
    {
        counter = 0;
        for (int i = 0; i < rightNails.Length; i++)
        {
            if (obj.name == rightNails[0].name || obj.name == leftNails[0].name)
            {
                return;
            }
            if (obj.name == rightNails[i].name)
            {
                for (int a = i; a < rightNails.Length; a++)
                {
                    if (rightNails[a].activeSelf == true)
                    {
                        counter++;
                    }
                }
                for (int j = i; j < rightNails.Length; j++)//a��k olanlara kadar hesaplay�p yap
                {
                    rightNails[j].SetActive(false);
                    leftNails[j].SetActive(false);
                    InstantiateParticle(i);
                }

            }
            if (obj.name == leftNails[i].name)
            {

                for (int a = i; a < rightNails.Length; a++)
                {
                    if (rightNails[a].activeSelf == true)
                    {
                        counter++;
                    }
                }
                for (int j = i; j < rightNails.Length; j++)
                {
                    rightNails[j].SetActive(false);
                    leftNails[j].SetActive(false);
                    InstantiateParticle(i);
                }
            }
        }
        if (nailStack != 0)
            nailStack -= counter;
    }
    private void InstantiateParticle(int value)
    {
        Instantiate(cutNailParticle, leftNails[value].transform.position, Quaternion.identity);
        Instantiate(cutNailParticle, rightNails[value].transform.position, Quaternion.identity);
    }
    public void FinishGame()
    {
        player.StopSwipeandRun();
        player.GetComponent<PlayerMovement>().WinDance();
        Invoke("WaitWinPanel", 2f);

    }
    public void GotoFinishLine()
    {
        player.transform.DOMoveX(0, 0.5f);
        player.LastRun();

    }
    public void WaitWinPanel()
    {
        UIManager.Instance.ShowPanel(PanelType.Win);

    }

}
