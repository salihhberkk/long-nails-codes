using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NailVarnish : MonoBehaviour
{
    public static int paintNumber = 0;
    MeshRenderer[] meshsLeft;
    MeshRenderer[] meshsRight;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Nail"))
        {
            meshsLeft = GameManager.Instance.leftNails[paintNumber].GetComponentsInChildren<MeshRenderer>();
            meshsRight = GameManager.Instance.rightNails[paintNumber].GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < meshsLeft.Length; i++)
            {
                meshsLeft[i].material = gameObject.GetComponent<MeshRenderer>().material;
            }
            for (int i = 0; i < meshsRight.Length; i++)
            {
                meshsRight[i].material = gameObject.GetComponent<MeshRenderer>().material;
            }

            paintNumber++;
        }

    }
}
