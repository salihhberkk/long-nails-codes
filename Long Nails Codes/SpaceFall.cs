using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceFall : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<PlayerMovement>();
        if (player != null)
        {
            player.Die();
        }
    }
}
