using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : MonoBehaviour
{
    [SerializeField] GameObject confettiEffect;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Instantiate(confettiEffect, transform.position + new Vector3(2f, 0, 0), Quaternion.identity);
            Instantiate(confettiEffect, transform.position + new Vector3(-2f, 0, 0), Quaternion.identity);
            GameManager.Instance.FinishGame();
        }
    }
}
